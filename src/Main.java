
import java.util.*;

public class Main {
    public static void main(String[] args) {
        int z = 5208;
        double C5 = z % 5;
        double C7 = z % 7;
        double C11 = z % 11;
        System.out.println("C5=" + C5 + "; C7=" + C7 + "; C11=" + C11 + ";");
		/* C5=% C7=double C11=Обчислити суму найбільших елементів в рядках матриці з непарними номерами та
		найменших елементів в рядках матриці з парними номерами*/
        int i, j;
        double S1 = 0, S2 = 0;

        Scanner mat = new Scanner(System.in);
        System.out.println("Enter the size of matrix A:");
        System.out.println("Height");
        int n = mat.nextInt();
        System.out.println("Width");
        int m = mat.nextInt();
        System.out.println("Enter the size of matrix B:");
        System.out.println("Height");
        int l = mat.nextInt();
        System.out.println("Width");
        int k = mat.nextInt();
        int e = n + l;
        int f = m + k;

        double A[][] = new double[n][m];
        double B[][] = new double[l][k];
        double C[][] = new double[e][f];

        System.out.println("Enter the elements of A:");
        for (i = 0; i < n; i++){
            for (j = 0; j < m; j++){
                System.out.println("A["+(i+1)+"."+(j+1)+"] =");
                A[i][j] = mat.nextDouble();
            }
        }
        System.out.println("Enter the elements of B:");
        for (i = 0; i < l; i++){
            for (j = 0; j < k; j++){
                System.out.println("B["+(i+1)+"."+(j+1)+"] =");
                B[i][j] = mat.nextDouble();
            }
        }

        for (i = 0; i < n; i++){
            for (j = 0; j < m; j++){
                C[i][j] = A[i][j];
            }
        }
        for (i = 0; i < l; i++){
            for (j = 0; j < k; j++){
                C[i+n][j+m] = B[i][j];
            }
        }

        System.out.println("Result of direct sum of A and B, the matrix C is:");
        for (i = 0; i < e; i++){
            for (j = 0; j < f; j++){
                System.out.print(C[i][j]+"  ");
            }
            System.out.println();
        }

        for (i = 0; i < e; i = i + 2){
            double max = C[i][0];
            for (j = 1; j < f; j++) {
                if (C[i][j] > max)
                    max = C[i][j];
            }
            S1 = S1 + max;
        }

        for (i = 1; i < e; i = i + 2){
            double min = C[i][0];
            for (j = 1; j < f; j++){
                if (C[i][j] < min)
                    min = C[i][j];
            }
            S2 = S2 + min;
        }

        System.out.println("Sum of max of odd numbers: "+S1);
        System.out.println("Sum of min of even numbers: "+S2);

    }
}